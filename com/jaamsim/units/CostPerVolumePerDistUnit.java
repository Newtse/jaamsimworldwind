package com.jaamsim.units;


public class CostPerVolumePerDistUnit extends Unit {
	static {
		Unit.setSIUnit(CostPerVolumePerDistUnit.class, "$/m3/m");
	}
	public CostPerVolumePerDistUnit() {
	}

}
