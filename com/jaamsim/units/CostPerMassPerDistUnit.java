package com.jaamsim.units;


public class CostPerMassPerDistUnit extends Unit {
	static {
		Unit.setSIUnit(CostPerMassPerDistUnit.class, "$/kg/m");
	}

	public CostPerMassPerDistUnit() {}

}
